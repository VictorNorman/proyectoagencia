﻿using System;
using System.Collections.Generic;

namespace Agencia.Models
{
    public partial class Sucursal
    {
        public Sucursal()
        {
            Ventas = new HashSet<Ventas>();
        }

        public int CodSucursal { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public int? CodPostal { get; set; }
        public string Calle { get; set; }
        public int? NoExterior { get; set; }

        public virtual ICollection<Ventas> Ventas { get; set; }
    }
}
