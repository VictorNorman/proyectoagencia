﻿using System;
using System.Collections.Generic;

namespace Agencia.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Ventas = new HashSet<Ventas>();
        }

        public int IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Calle { get; set; }
        public string Colonia { get; set; }

        public virtual ICollection<Ventas> Ventas { get; set; }
    }
}
