﻿using System;
using System.Collections.Generic;

namespace Agencia.Models
{
    public partial class Ventas
    {
        public int CodVenta { get; set; }
        public int? CodSucursal { get; set; }
        public int? IdEmpleado { get; set; }
        public int? IdCliente { get; set; }
        public int? NoSerie { get; set; }
        public decimal? Pago { get; set; }
        public string FormaPago { get; set; }

        public virtual Sucursal CodSucursalNavigation { get; set; }
        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual Empleados IdEmpleadoNavigation { get; set; }
        public virtual Autos NoSerieNavigation { get; set; }
    }
}
