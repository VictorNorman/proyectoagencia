﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Agencia.Models
{
    public partial class AgenciaContext : DbContext
    {
        public AgenciaContext()
        {
        }

        public AgenciaContext(DbContextOptions<AgenciaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Autos> Autos { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Empleados> Empleados { get; set; }
        public virtual DbSet<Sucursal> Sucursal { get; set; }
        public virtual DbSet<Ventas> Ventas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=Agencia;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autos>(entity =>
            {
                entity.HasKey(e => e.NoSerie)
                    .HasName("PK_No_Serie");

                entity.Property(e => e.NoSerie).HasColumnName("No_Serie");

                entity.Property(e => e.Color)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Marca)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoMotor).HasColumnName("No_Motor");

                entity.Property(e => e.TipoVehiculo)
                    .HasColumnName("Tipo_Vehiculo")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdCliente)
                    .HasName("PK_ID_Cliente");

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Calle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Colonia)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Municipio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Empleados>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado)
                    .HasName("PK_ID_Empleado");

                entity.Property(e => e.IdEmpleado).HasColumnName("ID_Empleado");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Calle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Colonia)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Municipio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Sucursal>(entity =>
            {
                entity.HasKey(e => e.CodSucursal)
                    .HasName("PK_CodSucursal");

                entity.Property(e => e.Calle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodPostal).HasColumnName("Cod_Postal");

                entity.Property(e => e.Estado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Municipio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoExterior).HasColumnName("No_Exterior");
            });

            modelBuilder.Entity<Ventas>(entity =>
            {
                entity.HasKey(e => e.CodVenta)
                    .HasName("PK_CodVenta");

                entity.Property(e => e.FormaPago)
                    .HasColumnName("Forma_Pago")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.IdEmpleado).HasColumnName("ID_Empleado");

                entity.Property(e => e.NoSerie).HasColumnName("No_Serie");

                entity.Property(e => e.Pago).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.CodSucursalNavigation)
                    .WithMany(p => p.Ventas)
                    .HasForeignKey(d => d.CodSucursal)
                    .HasConstraintName("FK__Ventas__CodSucur__31EC6D26");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Ventas)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK__Ventas__ID_Clien__33D4B598");

                entity.HasOne(d => d.IdEmpleadoNavigation)
                    .WithMany(p => p.Ventas)
                    .HasForeignKey(d => d.IdEmpleado)
                    .HasConstraintName("FK__Ventas__ID_Emple__32E0915F");

                entity.HasOne(d => d.NoSerieNavigation)
                    .WithMany(p => p.Ventas)
                    .HasForeignKey(d => d.NoSerie)
                    .HasConstraintName("FK__Ventas__No_Serie__34C8D9D1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
