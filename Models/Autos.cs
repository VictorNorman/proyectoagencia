﻿using System;
using System.Collections.Generic;

namespace Agencia.Models
{
    public partial class Autos
    {
        public Autos()
        {
            Ventas = new HashSet<Ventas>();
        }

        public int NoSerie { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int? Año { get; set; }
        public string Color { get; set; }
        public int? NoMotor { get; set; }
        public string TipoVehiculo { get; set; }

        public virtual ICollection<Ventas> Ventas { get; set; }
    }
}
