﻿using System;
using System.Linq;
using Agencia.Models;

namespace Agencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //CrearEmpleado();
            //BuscarEmpleado();
            //SelecionarEmpleado();
            //ActualizarEmpleado();
            //EliminarEmpleado();
            Menu();
            Console.Clear();
            Console.WriteLine("Sesión finalizada.");
        }

        public static void Menu()
        {
            Console.WriteLine("Menu");

            Console.WriteLine("1) Empleados");
            Console.WriteLine("2) Clientes");
            Console.WriteLine("3) Autos");
            Console.WriteLine("4) Sucursales");
            Console.WriteLine("5) Ventas");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    MenuEmpleados();
                    break;
                case "2":
                    MenuClientes();
                    break;
                case "3":
                    MenuAutos();
                    break;
                case "4":
                    MenuSucursales();
                    break;
                case "5":
                    MenuVentas();
                    break;
                case "0": return;
            }
            Console.Clear();
            Menu();
        }


        public static void MenuEmpleados()
        {
            Console.Clear();
            Console.WriteLine("Empleados");
            Console.WriteLine("1) Buscar Empleado");
            Console.WriteLine("2) Crear Empleado");
            Console.WriteLine("3) Eliminar Empleado");
            Console.WriteLine("4) Actualizar Empleado");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarEmpleado();
                    break;
                case "2":
                    CrearEmpleado();
                    break;
                case "3":
                    EliminarEmpleado();
                    break;
                case "4":
                    ActualizarEmpleado();
                    break;
                case "0": return;
            }

            MenuEmpleados();
        }

        public static void MenuClientes()
        {
            Console.Clear();
            Console.WriteLine("Clientes");
            Console.WriteLine("1) Buscar Cliente");
            Console.WriteLine("2) Crear Cliente");
            Console.WriteLine("3) Eliminar Cliente");
            Console.WriteLine("4) Actualizar Cliente");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarCliente();
                    break;
                case "2":
                    CrearCliente();
                    break;
                case "3":
                    EliminarCliente();
                    break;
                case "4":
                    ActualizarCliente();
                    break;
                case "0": return;
            }

            MenuClientes();
        }

        public static void MenuAutos()
        {
            Console.Clear();
            Console.WriteLine("Autos");
            Console.WriteLine("1) Buscar Auto");
            Console.WriteLine("2) Crear Auto");
            Console.WriteLine("3) Eliminar Auto");
            Console.WriteLine("4) Actualizar Auto");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarAuto();
                    break;
                case "2":
                    CrearAuto();
                    break;
                case "3":
                    EliminarAuto();
                    break;
                case "4":
                    ActualizarAuto();
                    break;
                case "0": return;
            }

            MenuAutos();
        }

        public static void MenuSucursales()
        {
            Console.Clear();
            Console.WriteLine("Sucursales");
            Console.WriteLine("1) Buscar Sucursal");
            Console.WriteLine("2) Crear Sucursal");
            Console.WriteLine("3) Eliminar Sucursal");
            Console.WriteLine("4) Actualizar Sucursal");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarSucursal();
                    break;
                case "2":
                    CrearSucursal();
                    break;
                case "3":
                    EliminarSucursal();
                    break;
                case "4":
                    ActualizarSucursal();
                    break;
                case "0": return;
            }

            MenuSucursales();
        }

        public static void MenuVentas()
        {
            Console.Clear();
            Console.WriteLine("Ventas");
            Console.WriteLine("1) Buscar Venta");
            Console.WriteLine("2) Crear Venta");
            Console.WriteLine("3) Eliminar Venta");
            Console.WriteLine("4) Actualizar Venta");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarVenta();
                    break;
                case "2":
                    CrearVenta();
                    break;
                case "3":
                    EliminarVenta();
                    break;
                case "4":
                    ActualizarVenta();
                    break;
                case "0": return;
            }

            MenuVentas();
        }

        public static void CrearEmpleado()
        {
            Console.Clear();
            Console.WriteLine("Nuevo Empleado");
            Empleados empleado = new Empleados();
            empleado = GuardarEmpleado(empleado);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Add(empleado);
                context.SaveChanges();
                Console.WriteLine("Empleado registrado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void CrearCliente()
        {
            Console.Clear();
            Console.WriteLine("Nuevo Cliente");
            Cliente cliente = new Cliente();
            cliente = GuardarCliente(cliente);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Add(cliente);
                context.SaveChanges();
                Console.WriteLine("Cliente registrado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void CrearAuto()
        {
            Console.Clear();
            Console.WriteLine("Nuevo Auto");
            Autos autos = new Autos();
            autos = GuardarAuto(autos);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Add(autos);
                context.SaveChanges();
                Console.WriteLine("Auto registrado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void CrearSucursal()
        {
            Console.Clear();
            Console.WriteLine("Nueva Sucursal");
            Sucursal sucursal = new Sucursal();
            sucursal = GuardarSucursal(sucursal);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Add(sucursal);
                context.SaveChanges();
                Console.WriteLine("Sucursal registrada, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void CrearVenta()
        {
            Console.Clear();
            Console.WriteLine("Nueva Venta\n\nPresion Enter.");
            Console.ReadLine();
            Ventas ventas = new Ventas();
            ventas = GuardarVenta(ventas);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Add(ventas);
                context.SaveChanges();
                Console.WriteLine("Venta registrada, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void BuscarEmpleado()
        {
            Console.Clear();
            Console.WriteLine("Buscar Empleado");
            Console.Write("Nombre: ");
            string buscar = Console.ReadLine();

            using (AgenciaContext context = new AgenciaContext())
            {
                IQueryable<Empleados> empleados = context.Empleados.Where(p => p.Nombre.Contains(buscar));
                foreach (Empleados empleado in empleados)
                {
                    Console.WriteLine($"{empleado.IdEmpleado}) {empleado.Nombre} {empleado.Apellido} Dir: {empleado.Estado}, {empleado.Municipio}, {empleado.Calle}, {empleado.Colonia}");
                }
                Console.WriteLine("Presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void BuscarCliente()
        {
            Console.Clear();
            Console.WriteLine("Buscar Cliente");
            Console.Write("Nombre: ");
            string buscar = Console.ReadLine();

            using (AgenciaContext context = new AgenciaContext())
            {
                IQueryable<Cliente> clientes = context.Cliente.Where(p => p.Nombre.Contains(buscar));
                foreach (Cliente cliente in clientes)
                {
                    Console.WriteLine($"{cliente.IdCliente}) {cliente.Nombre} {cliente.Apellido} Dir: {cliente.Estado}, {cliente.Municipio}, {cliente.Calle}, {cliente.Colonia}");
                }
                Console.WriteLine("Presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void BuscarAuto()
        {
            Console.Clear();
            Console.WriteLine("Buscar Auto");
            Console.Write("Marca: ");
            string buscar = Console.ReadLine();

            using (AgenciaContext context = new AgenciaContext())
            {
                IQueryable<Autos> autos = context.Autos.Where(p => p.Marca.Contains(buscar));
                foreach (Autos autos1 in autos)
                {
                    Console.WriteLine($"{autos1.NoSerie}) {autos1.Marca} {autos1.Modelo} {autos1.Año} Color: {autos1.Color}, No. Motor: {autos1.NoMotor}, Tipo: {autos1.TipoVehiculo}");
                }
                Console.WriteLine("Presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void BuscarSucursal()
        {
            Console.Clear();
            Console.WriteLine("Buscar Sucursal");
            Console.Write("Estado: ");
            string buscar = Console.ReadLine();

            using (AgenciaContext context = new AgenciaContext())
            {
                IQueryable<Sucursal> sucursals = context.Sucursal.Where(p => p.Estado.Contains(buscar));
                foreach (Sucursal sucursal in sucursals)
                {
                    Console.WriteLine($"{sucursal.CodSucursal}) {sucursal.Estado}, {sucursal.Municipio}, {sucursal.CodPostal}, {sucursal.Calle} No.Ext: {sucursal.NoExterior}");
                }
                Console.WriteLine("Presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void BuscarVenta()
        {
            Console.Clear();
            Console.WriteLine("Buscar Venta");
            Console.Write("Forma de pago: ");
            string buscar = Console.ReadLine();
            
            using (AgenciaContext context = new AgenciaContext())
            {                
                IQueryable<Ventas> ventas = context.Ventas.Where(p => p.FormaPago.Contains(buscar));
                foreach (Ventas ventas1 in ventas)
                {
                    Console.WriteLine($"{ventas1.CodVenta}) Sucursal: {ventas1.CodSucursal}, Empleado: {ventas1.IdEmpleadoNavigation}, Cliente: {ventas1.IdCliente}, No. Serie{ventas1.NoSerie}, Total: {ventas1.Pago}, Forma de pago: {ventas1.FormaPago}");
                }
                Console.WriteLine("Presione ENTER.");
                Console.ReadLine();
            }
        }

        public static Empleados SelecionarEmpleado()
        {
            Console.Clear();
            BuscarEmpleado();
            Console.Write("Selecione Id de Empleado: ");
            int id = int.Parse(Console.ReadLine());
            using (AgenciaContext context = new AgenciaContext())
            {
                Empleados empleado = context.Empleados.Find(id);
                if (empleado == null)
                {
                    SelecionarEmpleado();
                }
                return empleado;
            }
        }

        public static Cliente SelecionarCliente()
        {
            Console.Clear();
            BuscarCliente();
            Console.Write("Selecione Id de Cliente: ");
            int id = int.Parse(Console.ReadLine());
            using (AgenciaContext context = new AgenciaContext())
            {
                Cliente cliente = context.Cliente.Find(id);
                if (cliente == null)
                {
                    SelecionarCliente();
                }
                return cliente;
            }
        }

        public static Autos SelecionarAuto()
        {
            Console.Clear();
            BuscarAuto();
            Console.Write("Selecione No. Serie: ");
            int id = int.Parse(Console.ReadLine());
            using (AgenciaContext context = new AgenciaContext())
            {
                Autos autos = context.Autos.Find(id);
                if (autos == null)
                {
                    SelecionarAuto();
                }
                return autos;
            }
        }

        public static Sucursal SelecionarSucursal()
        {
            Console.Clear();
            BuscarSucursal();
            Console.Write("Selecione un Código de Sucursal: ");
            int id = int.Parse(Console.ReadLine());
            using (AgenciaContext context = new AgenciaContext())
            {
                Sucursal sucursal = context.Sucursal.Find(id);
                if (sucursal == null)
                {
                    SelecionarSucursal();
                }
                return sucursal;
            }
        }

        public static Ventas SelecionarVenta()
        {
            Console.Clear();
            BuscarVenta();
            Console.Write("Selecione un Código de Venta: ");
            int id = int.Parse(Console.ReadLine());
            using (AgenciaContext context = new AgenciaContext())
            {
                Ventas ventas = context.Ventas.Find(id);
                if (ventas == null)
                {
                    SelecionarSucursal();
                }
                return ventas;
            }
        }

        public static Empleados GuardarEmpleado(Empleados empleado)
        {
            Console.Write("Nombre: ");
            empleado.Nombre = Console.ReadLine();
            Console.Write("Apellido: ");
            empleado.Apellido = Console.ReadLine();
            Console.Write("Estado: ");
            empleado.Estado = Console.ReadLine();
            Console.Write("Municipio: ");
            empleado.Municipio = Console.ReadLine();
            Console.Write("Calle: ");
            empleado.Calle = Console.ReadLine();
            Console.Write("Colonia: ");
            empleado.Colonia = Console.ReadLine();
            return empleado;
        }

        public static Cliente GuardarCliente(Cliente cliente)
        {
            Console.Write("Nombre: ");
            cliente.Nombre = Console.ReadLine();
            Console.Write("Apellido: ");
            cliente.Apellido = Console.ReadLine();
            Console.Write("Estado: ");
            cliente.Estado = Console.ReadLine();
            Console.Write("Municipio: ");
            cliente.Municipio = Console.ReadLine();
            Console.Write("Calle: ");
            cliente.Calle = Console.ReadLine();
            Console.Write("Colonia: ");
            cliente.Colonia = Console.ReadLine();
            return cliente;
        }

        public static Autos GuardarAuto(Autos autos)
        {
            Console.Write("Marca: ");
            autos.Marca = Console.ReadLine();
            Console.Write("Modelo: ");
            autos.Modelo = Console.ReadLine();
            Console.Write("Año: ");
            autos.Año = int.Parse(Console.ReadLine());
            Console.Write("Color: ");
            autos.Color = Console.ReadLine();
            Console.Write("No. Motor: ");
            autos.NoMotor = int.Parse(Console.ReadLine());
            Console.Write("Tipo de Vehículo: ");
            autos.TipoVehiculo = Console.ReadLine();
            return autos;
        }

        public static Sucursal GuardarSucursal(Sucursal sucursal)
        {
            Console.Write("Estado: ");
            sucursal.Estado = Console.ReadLine();
            Console.Write("Municipio: ");
            sucursal.Municipio = Console.ReadLine();
            Console.Write("C.P: ");
            sucursal.CodPostal = int.Parse(Console.ReadLine());
            Console.Write("Calle: ");
            sucursal.Calle = Console.ReadLine();
            Console.Write("No. Exteriór: ");
            sucursal.NoExterior = int.Parse(Console.ReadLine());
            return sucursal;
        }

        public static Ventas GuardarVenta(Ventas ventas)
        {
            BuscarSucursal();
            Console.Write("Selecione un Código de Sucursal: ");
            int codSucur = int.Parse(Console.ReadLine());
            ventas.CodSucursal = codSucur;

            BuscarEmpleado();
            Console.Write("Selecione Id de Cliente: ");
            int idEmp = int.Parse(Console.ReadLine());
            ventas.IdEmpleado = idEmp;

            BuscarCliente();
            Console.Write("Selecione Id de Cliente: ");
            int idClt = int.Parse(Console.ReadLine());
            ventas.IdCliente = idClt;

            BuscarAuto();
            Console.Write("Selecione No. Serie: ");
            int noSerie = int.Parse(Console.ReadLine());
            ventas.NoSerie = noSerie;

            Console.Clear();
            Console.Write("Suma Total: ");
            ventas.Pago = decimal.Parse(Console.ReadLine());

            Console.Write("Forma de pago: ");            
            ventas.FormaPago = Console.ReadLine();

            return ventas;
        }

        public static void ActualizarEmpleado()
        {
            Console.Clear();
            Console.WriteLine("Actualizar Empleado");
            Empleados empleado = SelecionarEmpleado();
            empleado = GuardarEmpleado(empleado);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Update(empleado);
                context.SaveChanges();
                Console.WriteLine("Empleado Actualizado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void ActualizarCliente()
        {
            Console.Clear();
            Console.WriteLine("Actualizar Cliente");
            Cliente cliente = SelecionarCliente();
            cliente = GuardarCliente(cliente);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Update(cliente);
                context.SaveChanges();
                Console.WriteLine("Empleado Actualizado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void ActualizarAuto()
        {
            Console.Clear();
            Console.WriteLine("Actualizar Auto");
            Autos autos = SelecionarAuto();
            autos = GuardarAuto(autos);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Update(autos);
                context.SaveChanges();
                Console.WriteLine("Auto Actualizado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void ActualizarSucursal()
        {
            Console.Clear();
            Console.WriteLine("Actualizar Sucursal");
            Sucursal sucursal = SelecionarSucursal();
            sucursal = GuardarSucursal(sucursal);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Update(sucursal);
                context.SaveChanges();
                Console.WriteLine("Sucursal Actualizada, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void ActualizarVenta()
        {
            Console.Clear();
            Console.WriteLine("Actualizar Venta");
            Ventas ventas = SelecionarVenta();
            ventas = GuardarVenta(ventas);
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Update(ventas);
                context.SaveChanges();
                Console.WriteLine("Sucursal Actualizada, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void EliminarEmpleado()
        {
            Console.Clear();
            Console.WriteLine("Eliminar Empleado");
            Empleados empleado = SelecionarEmpleado();
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Remove(empleado);
                context.SaveChanges();
                Console.WriteLine("Empleado eliminado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void EliminarCliente()
        {
            Console.Clear();
            Console.WriteLine("Eliminar Cliente");
            Cliente cliente = SelecionarCliente();
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Remove(cliente);
                context.SaveChanges();
                Console.WriteLine("Cliente eliminado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void EliminarAuto()
        {
            Console.Clear();
            Console.WriteLine("Eliminar Auto");
            Autos autos = SelecionarAuto();
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Remove(autos);
                context.SaveChanges();
                Console.WriteLine("Auto eliminado, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void EliminarSucursal()
        {
            Console.Clear();
            Console.WriteLine("Eliminar Sucursal");
            Sucursal sucursal = SelecionarSucursal();
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Remove(sucursal);
                context.SaveChanges();
                Console.WriteLine("Sucursal eliminada, presione ENTER.");
                Console.ReadLine();
            }
        }

        public static void EliminarVenta()
        {
            Console.Clear();
            Console.WriteLine("Eliminar Venta");
            Ventas ventas = SelecionarVenta();
            using (AgenciaContext context = new AgenciaContext())
            {
                context.Remove(ventas);
                context.SaveChanges();
                Console.WriteLine("Sucursal eliminada, presione ENTER.");
                Console.ReadLine();
            }
        }
    }
}
